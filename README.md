# haskell stack musl
This is a simple docker image based on alpine to provide a build environment for static linking haskell programs with musl

## Image Status

| Tool  | Version |
| ----  | ------- |
| ghc   | 8.4.3   |
| stack | 2.1.3   |
| cabal | 2.4.1.0 |
| hpack | 0.33.0  |

## Links
[git repo](https://gitlab.com/ejectedspace/docker/stack-musl/)  
[dockerhub](https://cloud.docker.com/u/ejectedspace/repository/docker/ejectedspace/stack-musl)  

Use this image:  
`docker pull cocainefarm/stack-musl`

## About this image
The image itself is rather simple. We install most needed build tools including `ghc`, `cabal`, `hpack` and `stack`.
While testing the image I ran into problems compiling with stack and it's own ghc. While stack itself provides a static binary the ghc version it downloads is not static and does not work in alpine.
This is also the reason I provide my own *static* `hpack` executable.

## Usuall workflow
As stated above directly compiling with stack does not work.  
To circumvent this I have bundled hpack for generating a `.cabal` file from your `package.yml`
So for your usuall project build with stack we would have a Dockerfile like this:
As already stated above we can't use stack, so I resorted to generating the `.cabal` file myself with hpack and just building it with cabal and the system ghc version

```
FROM cocainefarm/stack-musl as builder

# Add the main dir to the container and set the working dir to it
ADD . /usr/src/build
WORKDIR /usr/src/build

# generate the .cabal file
RUN hpack

# update the package list
RUN cabal new-update

# build the binary and place it into a known location for ease of access
# with the ghc flags needed for static compiling with optimisation
RUN cabal install --ghc-options='-optl-static -fPIC -optc-Os'

# Copy the binary over to a empty docker container
# this is the container that will be served
FROM scratch
#    set src        bin path in src                  bin path dest
COPY --from=builder /root/.cabal/bin/<your bin here> /<your bin here>
CMD ["/<your bin here>"]
```

